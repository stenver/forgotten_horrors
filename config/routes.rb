Vikings::Application.routes.draw do

  get "/culture", to: "static_pages#culture"
  get "/gods", to: "static_pages#gods"
  get "/game_rules", to: "static_pages#game_rules"
  get "/timeline", to: "static_pages#timeline"
  get "/map", to: "static_pages#map"
  get "/strategic_map", to: "static_pages#strategic_map"
  get "/hideout", to: "static_pages#hideout"
  get "/homerules", to: "static_pages#homerules"
  root "static_pages#home"
end

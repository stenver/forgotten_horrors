class StaticPagesController < ApplicationController
  def home
  end

  def gods
  end

  def game_rules
  end

  def map
  end

  def strategic_map
  end

  def hideout
  end
end
